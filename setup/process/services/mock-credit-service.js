const axios = require("axios");

async function lookUpIndividual(nationalId) {
  if (!nationalId) {
    throw Error("National id must be specified");
  }
  const { data } = await axios.get("https://randomuser.me/api/");
  const [individual] = data.results;
  const firstDigit = parseInt(nationalId[0]);
  return {
    individual,

    //Credit policy: even first digit = approve
    creditCheck: { decision: firstDigit % 2 === 0 ? "APPROVE" : "DECLINE" }
  };
}

module.exports = { lookUpIndividual };
