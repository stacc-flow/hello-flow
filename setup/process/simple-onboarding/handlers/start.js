module.exports = {
  handle: ({ input }) => {
    return {
      state: {
        application: {
          nationalId: input.nationalId,
          approveCreditCheck: input.approveCreditCheck
        }
      },
      variables: {}
    };
  }
};
