module.exports = {
  getFlowPermissions: async ({ user, state }) => {
    return {
      read: true,
      create: true,
      save: true,
      delete: true,
      assign: true,
      unassign: true
    };
  }
};
