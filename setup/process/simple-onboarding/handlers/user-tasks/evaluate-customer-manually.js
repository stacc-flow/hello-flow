module.exports = {
  execute: async ({ input }) => {
    return input;
  },
  map: async ({ data }) => {
    return {
      patch: [{ op: "replace", path: "/manualEvaluation", value: data }],
      variables: { isManuallyApproved: data.isManuallyApproved }
    };
  },
  getContext: async ({ state }) => {
    const { customer, creditCheck } = state;
    return {
      customer,
      creditCheck
    };
  }
};
