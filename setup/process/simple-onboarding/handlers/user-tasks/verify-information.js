module.exports = {
  execute: async ({ input }) => {
    return input;
  },
  map: async ({ data }) => {
    return { patch: [{ op: "replace", path: "/verification", value: data }], variables: {} };
  },
  getContext: async ({ state }) => {
    const { customer } = state;
    const { name, birthdate, address, postalcode, postalplace } = customer;
    return { name, birthdate, address, postalcode, postalplace };
  }
};
