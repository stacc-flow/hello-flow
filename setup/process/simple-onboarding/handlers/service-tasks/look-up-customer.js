const service = require("../../../services/mock-credit-service");

module.exports = {
  execute: async ({ state }) => {
    const { creditCheck, individual } = await service.lookUpIndividual(state.application.nationalId);
    return { creditCheck, individual };
  },
  map: async ({ data }) => {
    const isApproved = data.creditCheck.decision === "APPROVE";
    return {
      patch: [
        { op: "replace", path: "/creditCheck", value: data.creditCheck },
        { op: "replace", path: "/customer", value: data.individual }
      ],
      variables: { isApproved }
    };
  }
};
