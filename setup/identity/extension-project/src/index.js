// This function creates the identifier which will become the claim 'sub' in the identity token.
// This function is named in a slightly weird way, because it makes sense in services-identity.
function GetUserIdFromExternal(callback, data) {
  const subClaim = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";

  if (data.provider === "username-password-provider") {
    // We use the username as the subclaim for now - this is subject (lol) to change later.
    const username = data.externalClaims.find(claim => claim.type === subClaim).value;
    return callback(null, username);
  }

  if (data.provider === "signicat-nbid") {
    // We use the username as the subclaim for now - this is subject (lol) to change later.
    const national_id = data.externalClaims.find(claim => claim.type === "national_id").value;
    return callback(null, national_id);
  }

  return callback("Login provider not supported.", null);
}

function UserSignedInFromExternal(callback, data) {
  return callback(null, { localClaims: [] });
}

function OpenIdConnectOnRedirectToProvider(callback, request) {
  if (request.provider === "signicat-nbid") {
    return callback(null, { AcrValues: "urn:signicat:oidc:method:nbid" }); //nbid refers to [n]orwegian [b]ank[id]
  }
  return callback(null, { AcrValues: "" });
}

function ProfileIsActive(callback, data) {
  return callback(null, { isActive: true });
}

function ProfileGetData(callback, data) {
  if (data.clientName === "portal") {
    const issuedClaims = [
      { type: "role", value: "admin" },
      { type: "username", value: data.subject },
      { type: "name", value: data.subject }
    ];
    return callback(null, {
      filterClaims: true,
      issuedClaims
    });
  }

  return callback(null, {
    filterClaims: true,
    issuedClaims: []
  });
}

module.exports = {
  GetUserIdFromExternal,
  UserSignedInFromExternal,
  OpenIdConnectOnRedirectToProvider,
  ProfileGetData,
  ProfileIsActive
};
