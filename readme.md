# readme

## simple hello world setup to test new api gw config.

validation and bundle scripts available, see package.json

 !! OAP config must be renamed before pushing up
API-GW only accepts one config file at a time and the filename must be 'config' 

Remember to rename bundle before pushing out!

> does flow push respect ignored files? ..feature?


----


## Questions
What part of service api config should be abstracted out.
There are different extremes here, but if we are adding a service then this service already has edpoints set up. Meaning that we want to copy over the service API config and then add these endpoints to our master config via refs. Bundler then resolves refs.

Interactively add endpoints?
    What if service has 10 endpoints and you only want a subset
    what if you only want to permit certain operations on a endpoint
    how will the custom parts be added to this?
        handlers
        auth
    

## Proposal

**AIM:** entire GW config setup via cli.
> while the swagger config is readable, its simply too much and too slow to set up manually. While the validator catches some mistakes, it is very easy to have other slips. These might not be caught util the config is deployed, thus leading to a somewhat long feedback loop.
> We can this risk by walking the user through a GW setup in the cli.


### Present the user with a step-like process via cli. 

**The main activities being:**
1. Declare defaults such as the info object, servers, tags, etc.
2. import service api descriptions by selecting services from menu
3. user can then choose to interactively walk through every service, endpoint, and operation and choose to delete it if not needed.
> The service api description will be absolute and complete. It must describe all endpoints and operations availabel on that service. I imagine that when creating a new gateway we might not need all endpoints and operations of a selecte service. These will therefore need to be removed. By removing them before we start adding proxies, authentication, or handlers, we are reducing complexity.
4. declare proxy and add proxies to endpoint
   > user first declares the proxies on root.  
   user can then add proxy reference on endpoint. 
5. add authentication
   > user first declares authentication on root. 
   user can then add authentication on endpoint and operational level
6. add handlers
   > user is prompted with a list of available handlers and can choose. Handler reference is then added and the handler skeleton is added to the external handler file. User then only has to fill in the actual code afterwards
    

The cli must also have to support this retroactively. If i discover that i need to add another service, add some handlers, remove some authentication, etc. Then i simply have to point at the config and it will be parsed and can then be edited via the cli. 

Goal should be that the cli can effectively by used as a proxy for any actions that need to be performed on the config.
    
After user has set up the config it needs to be validated according to the official spec, but also in accorance with the extensions and customizations that we have.

 




